<?php
namespace DemoShop;
/**
 * Created by PhpStorm.
 * User: thefish
 * Date: 08.04.15
 * Time: 14:05
 * @var $orders Order[]
 */
$response = array();
foreach ($orders as $order) {
    $responseOrder = array();
    $responseOrder['orderId'] = $order->id;
    $total = 0;
    foreach ($order->getOrderedProducts() as $orderProduct) {

        $tags = array();

        // Категории
        $tags['category'][] = $orderProduct->getProduct()->getCategory()->name;
        foreach ($orderProduct->getProduct()->getCategory()->getAllParents() as $category) {
            $tags['category'][] = $category->name;
        }
        //Какие-то еще теги
        $left_foot_wish = array(1 => 'left', 2 => 'right');
        $tags['left_foot_wish'][] = $left_foot_wish[array_rand($left_foot_wish)];
        //Корзина
        $responseOrder['cart'][] = array(
            'id' => $orderProduct->id,
            'name' => $orderProduct->getProduct()->name,
            'qty' => (int)$orderProduct->count,
            'price' => (float)$orderProduct->getProduct()->price,
            'tags' => $tags
        );


        $total = $total + ((int)$orderProduct->count * (float)$orderProduct->getProduct()->price);
    }
//Общая сумма заказа
    $responseOrder['total'] = $total;
//Статус заказа
    $responseOrder['status'] = (int)$order->status;

    $response[] = $responseOrder;
}

echo json_encode($response);